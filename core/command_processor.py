# -*- coding: utf-8 -*-

#  Copyright (c) 2025.
#
#  Created by AnyKeyShik Rarity
#
#  Telegram: @AnyKeyShik
#  GitHub: https://github.com/AnyKeyShik
#  E-mail: nikitav59@gmail.com

import os
import random

from utils import json_handler, debug, info


class CommandProcessor(object):
    def __init__(self):
        self.TAG = "CommandProcessor"

    def start(self):
        """
        Return start answer by '/start' command

        :return: start answer
        :rtype: str
        """

        info(self.TAG, "Start answer")
        return json_handler.messages['start_answer']

    def hello(self):
        """
        Return hello answer by 'hello' message

        :return: hello answer
        :rtype: str
        """

        info(self.TAG, "Hello answer")
        return json_handler.messages['hello_answer']

    def kek(self):
        """
        Just KEK

        :return: KEK
        :rtype: str
        """

        info(self.TAG, "KEK")
        return json_handler.messages['kek']

    def random_kek(self):
        """
        Send random phrase about kek

        :return: random phrase
        :rtype: str
        """

        info(self.TAG, "Random KEK")
        choices = json_handler.messages['random_kek']
        num = random.randint(1, 100 * len(choices))
        return choices[num // 100]

    def random_sticker(self, stickers):
        """
        Send random sticker

        :return: sticker id
        :rtype: str
        """

        info(self.TAG, "Random KEK")
        num = random.randint(1, 100 * len(stickers))
        return stickers[num // 100].file_id

    def roll(self, argument):
        """
        Choice of provided options

        :return: one of many
        :rtype: str
        """

        info(self.TAG, "roll")

        if len(argument) < 6:
            return json_handler.messages['no_choices_answer']

        debug(self.TAG, "Argument for roll: " + argument)
        sentence = argument[argument.find(' ') + 1:]
        choices = sentence.split("или")

        while choices.count("или") != 0:
            choices.remove("или")

        for index, word in enumerate(choices):
            if word.find(" ") == 0:
                choices[index] = word.replace(" ", "", 1)
            if word.rfind(" ") == len(word) - 1:
                choices[index] = word[:len(word) - 1]

        choices = list(filter(None, choices))

        debug(self.TAG, "Available choices: " + str(choices))

        if len(choices) > 1:

            num = random.randint(1, 100 * len(choices))

            return choices[num // 100]
        else:
            return json_handler.messages['no_choices_answer']

    def pc_status(self, argument, user_id):
        """
        Get pc status

        :return: message about pc status
        :rtype: str
        """

        info(self.TAG, "PC status")
        
        if user_id not in json_handler.hw_constants['users'].keys():
            return self.random_kek()
        
        if argument.find(' ') != -1:
            pc_name = argument[argument.find(' ') + 1:]
        else:
            pc_name = None

        if pc_name is None:
            pc_name = (json_handler.hw_constants['users'])[user_id]
        elif pc_name not in json_handler.hw_constants['pc_ips'].keys():
            return json_handler.messages['unknown_pc']
        
        pc_ip = (json_handler.hw_constants['pc_ips'])[pc_name]

        res = os.system("timeout 0.2 ping -c 1 " + pc_ip)
        if res == 0:
            return pc_name + json_handler.messages['pc_is_on']
        else:
            return pc_name + json_handler.messages['pc_is_off']

    def pc_wakeup(self, argument, user_id):
        """
        Wake up pc

        :return: message about operation
        :rtype: str
        """

        info(self.TAG, "PC wakeup")
        
        if user_id not in json_handler.hw_constants['users'].keys():
            return self.random_kek()

        if argument.find(' ') != -1:
            pc_name = argument[argument.find(' ') + 1:]
        else:
            pc_name = None
        
        if pc_name is None:
            pc_name = (json_handler.hw_constants['users'])[user_id]
        elif pc_name not in json_handler.hw_constants['pc_hw'].keys():
            return json_handler.messages['unknown_pc']
        
        pc_hw = (json_handler.hw_constants['pc_hw'])[pc_name]

        os.system("awake " + pc_hw)
        return pc_name + json_handler.messages['pc_now_wake']
