# -*- coding: utf-8 -*-

#  Copyright (c) 2025.
#
#  Created by AnyKeyShik Rarity
#
#  Telegram: @AnyKeyShik
#  GitHub: https://github.com/AnyKeyShik
#  E-mail: nikitav59@gmail.com

class CommandNotFoundException(Exception):
    pass
