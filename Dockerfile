FROM python:3.12
COPY . /Haruhi
WORKDIR /Haruhi
RUN apt-get update -y
RUN apt-get install -y iputils-ping
RUN pip3 install -r requirements.txt
ENV HARUHI_HOME="/Haruhi"
RUN mkdir /Haruhi/logs
ENTRYPOINT ["python", "./run.py"]
