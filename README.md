# Haruhi bot

Bot for kek and control home

# Now:
* PC power control
* KEK
* Choice from several variants


# Getting started

#### Requirements

To run this project, you will need:
* pytelegrambotapi
* PySocks (optional, for proxy if needed)
* Python3

#### Deploy

##### Without Docker
1. Fill constants for your bot
2. Install requirements
3. Run `run.py` 

##### With Docker
1. Fill constants for your bot
2. Build image (`docker build -t <image_name> .`)
3. Create folder for logs
4. Start container with your folder (`docker run --restart=always -d -v <your_folder>:/Haruhi/logs -t <image_name>`)
