# -*- coding: utf-8 -*-

#  Copyright (c) 2025.
#
#  Created by AnyKeyShik Rarity
#
#  Telegram: @AnyKeyShik
#  GitHub: https://github.com/AnyKeyShik
#  E-mail: nikitav59@gmail.com

from core import CommandProcessor
from utils import debug

cp = CommandProcessor()
TAG = 'LOCAL HANDLER'


def start():
    while True:
        message = input("> ")
        process(message)


def process(message):
    debug(TAG, "Get message '" + message + "'")

    if message.find('/start') != -1:
        start_message()
    elif message.find('/kek') != -1:
        kek_message()
    elif message.find('/roll') != -1:
        roll_message(message)
    elif message.find('/status') != -1:
        pc_status(message, "369579223")
    elif message.find('/wakeup') != -1:
        pc_wakeup(message, "369579223")
    else:
        send_text(message)


def start_message():
    print(cp.start())


def kek_message():
    for i in range(10):
        print(cp.kek())


def roll_message(message):
    print(cp.roll(message))


def pc_status(message, user_id):
    print(cp.pc_status(message, user_id))


def pc_wakeup(message, user_id):
    print(cp.pc_wakeup(message, user_id))


def send_text(message):
    if message == 'Привет':
        print(cp.hello())
    else:
        print(cp.random_kek())
